const stpSteps = document.querySelectorAll(".stp");
const circleElements = document.querySelectorAll(".circle");
const nextStepButton = document.querySelectorAll(".next-stp");
const prevStepButton = document.querySelectorAll(".prev-stp");
const myForm = document.getElementById("myForm");
const formInputs = document.querySelectorAll(".step-1 form input");
circleElements[0].style.background = "hsl(0, 0%, 100%)";
circleElements[0].style.color = "black";
const plans = document.querySelectorAll(".plan-card");
const extra = document.querySelectorAll(".price");
let element = "";
let price = "";
let switchBoolean = false;
let count = 9;

const obj = {
  plan: null,
  kind: null,
  price: null,
};

myForm.addEventListener("submit", function (event) {
  // Prevent the default form submission behavior
  event.preventDefault();

  // Your validation or form submission code can go here

  // For example, you can log a message when the form is submitted
  console.log("Form submitted!");
});

function getCurrentStep(validation) {
  for (let index = 0; index < stpSteps.length - 1; index++) {
    if (validation) {
      if (window.getComputedStyle(stpSteps[index]).display === "flex") {
        stpSteps[index].style.display = "none";
        if (index < 3) {
          circleElements[index].style.background = "transparent";
          circleElements[index].style.color = "white";
        }

        stpSteps[index + 1].style.display = "flex";
        if (index < 3) {
          circleElements[index + 1].style.background = "white";
          circleElements[index + 1].style.color = "black";
        }

        return;
      }
    } else {
      if (window.getComputedStyle(stpSteps[index]).display === "flex") {
        stpSteps[index].style.display = "none";

        circleElements[index].style.background = "transparent";
        circleElements[index].style.color = "white";

        stpSteps[index - 1].style.display = "flex";

        circleElements[index - 1].style.background = "white";
        circleElements[index - 1].style.color = "black";

        return;
      }
    }
  }
}

nextStepButton.forEach((nextButton) => {
  nextButton.addEventListener("click", function (event) {
    console.log("button clicked");
    if (validateForm()) {
      getCurrentStep(true);
    } else {
      showError(formInputs, "please enter required fields");
    }
    AddOns(switchBoolean);
    totalSummary(element, price, switchBoolean);
  });
});

prevStepButton.forEach((prevButton) => {
  prevButton.addEventListener("click", () => {
    console.log("button clicked");
    getCurrentStep(false);
  });
});

function validateForm() {
  let valid = true;

  if (formInputs[0].value.trim() === "") {
    valid = false;
  } else if (!isValidEmail(formInputs[1].value)) {
    valid = false;
  } else if (!isValidPhoneNumber(formInputs[2].value)) {
    valid = false;
  }

  return valid;
}

function isValidEmail(email) {
  return /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email);
}

function isValidPhoneNumber(phoneNumber) {
  return /^[+\d\s]+$/.test(phoneNumber);
}

function showError(input, errorMessage) {
  const errorLabel = document.querySelectorAll(".error");
  console.log(errorLabel);
  errorLabel.forEach((errorLabel) => {
    errorLabel.textContent = errorMessage;
    errorLabel.style.display = "block";
  });
}

const checkbox = document.querySelector(".switch");
function priceUpdater(checked) {
  console.log("hello");
  const yearlyPrice = [90, 120, 150];
  const monthlyPrice = [9, 12, 15];
  const prices = document.querySelectorAll(".plan-priced");
  const year = "2 months free";
  if (checked) {
    prices[0].textContent = `$${yearlyPrice[0]}/yr`;
    prices[0].appendChild(document.createElement("br"));
    const yearlyPriceElement = document.createElement("span");
    yearlyPriceElement.textContent = `${year}`;
    prices[0].appendChild(yearlyPriceElement);
    yearlyPriceElement.classList.add("monthfree");
    yearlyPriceElement.style.color = "black";
    prices[1].textContent = `$${yearlyPrice[1]}/yr`;
    prices[1].appendChild(document.createElement("br"));

    const yearlyPriceElement1 = document.createElement("span");
    yearlyPriceElement1.textContent = `${year}`;
    prices[1].appendChild(yearlyPriceElement1);
    yearlyPriceElement1.classList.add("monthfree");
    yearlyPriceElement1.style.color = "black";

    prices[2].textContent = `$${yearlyPrice[2]}/yr`;
    prices[2].appendChild(document.createElement("br"));
    const yearlyPriceElement2 = document.createElement("span");
    yearlyPriceElement2.textContent = `${year}`;
    prices[2].appendChild(yearlyPriceElement2);
    yearlyPriceElement2.classList.add("monthfree");
    yearlyPriceElement2.style.color = "black";
  } else {
    prices[0].textContent = `$${monthlyPrice[0]}/mo `;
    prices[1].textContent = `$${monthlyPrice[1]}/mo`;
    prices[2].textContent = `$${monthlyPrice[2]}/mo`;
  }
}
checkbox.addEventListener("click", () => {
  console.log("hellllo");
  const val = checkbox.querySelector("input").checked;
  if (val) {
    document.querySelector(".monthly").classList.remove("sw-active");
    document.querySelector(".yearly").classList.add("sw-active");
  } else {
    document.querySelector(".monthly").classList.add("sw-active");
    document.querySelector(".yearly").classList.remove("sw-active");
  }
  priceUpdater(val);

  switchBoolean = val;
  AddOns(switchBoolean);
});

plans.forEach((plan) => {
  plan.addEventListener("click", () => {
    document.querySelector(".selected").classList.remove("selected");
    plan.classList.add("selected");
    element = plan.querySelector("b").textContent;
    price = plan.querySelector(".plan-priced").textContent;

    // console.log(obj);
  });
});
const monthly = ["+$1/mo", "+$2/mo", "+$2/mo"];
const yearly = ["+$10/yr", "+$20/yr", "+$20/yr"];

const extras = document.querySelectorAll(".price");

function AddOns(selection) {
  if (selection) {
    extras[0].textContent = yearly[0];
    extras[1].textContent = yearly[1];
    extras[2].textContent = yearly[2];
  } else {
    extras[0].textContent = monthly[0];
    extras[1].textContent = monthly[1];
    extras[2].textContent = monthly[2];
  }
}
const mainPlan = document.querySelector(".plan-name");
const mainPrice = document.querySelector(".plan-price");
function totalSummary(name, price, bool) {
  if (bool) {
    if (name.length === 0) {
      name = "Arcade(Yearly)";
      price = "$90/yr ";
    } else {
      name += "(Yearly)";
    }
    mainPlan.textContent = name;
    mainPrice.textContent = price.slice(0, 7);
  } else {
    if (name.length === 0) {
      name = "Arcade(Monthly)";
      price = "$9/mo ";
    } else {
      name += "(Monthly)";
    }
    mainPlan.textContent = name;
    mainPrice.textContent = price.slice(0, 6);
  }
}

const undo = document.querySelector(".change");
undo.addEventListener("click", () => {
  stpSteps[3].style.display = "none";
  stpSteps[1].style.display = "flex";
});

// Get all checkboxes with the class "box-checkbox"
let checkboxes = document.querySelectorAll('.box input[type="checkbox"]');
const priceDataMonthly = [1, 2, 2];
const priceDataYearly = [10, 20, 20];
// Loop through each checkbox and attach an event listener
// Initialize an object to keep track of selected addon containers
const selectedAddons = {};

checkboxes.forEach(function (checkbox) {
  checkbox.addEventListener("change", function (event) {
    const monthly = ["+$1/mo", "+$2/mo", "+$2/mo"];
    const yearly = ["+$10/yr", "+$20/yr", "+$20/yr"];

    // Get the parent div with class "box" for the clicked checkbox
    let parentDiv = event.target.closest(".box");
    let dataId = parentDiv.getAttribute("data-id");
    let priceElement = parentDiv.querySelector("p.price");
    let labelElement = parentDiv.querySelector("label").textContent;

    if (event.target.checked) {
      if (dataId == "1") {
        // Update count and selected addon container
        if (dataId == "1" && priceElement.textContent.includes("yr")) {
          count += 10;
          updater(count, labelElement, yearly[0], true, dataId);
        } else {
          count += 1;
          updater(count, labelElement, monthly[0], true, dataId);
        }
      } else if (dataId == "2") {
        // Update count and selected addon container
        if (dataId == "2" && priceElement.textContent.includes("yr")) {
          count += 20;
          updater(count, labelElement, yearly[1], true, dataId);
        } else {
          count += 2;
          updater(count, labelElement, monthly[1], true, dataId);
        }
      } else if (dataId == "3") {
        // Update count and selected addon container
        if (dataId == "3" && priceElement.textContent.includes("yr")) {
          count += 20;
          updater(count, labelElement, yearly[2], true, dataId);
        } else {
          count += 2;
          updater(count, labelElement, monthly[2], true, dataId);
        }
      }
    } else {
      if (dataId in selectedAddons) {
        if (dataId == "1") {
          if (dataId == "1" && priceElement.textContent.includes("yr")) {
            count -= 10;
          } else {
            count -= 1;
          }
        } else if (dataId == "2") {
          if (dataId == "2" && priceElement.textContent.includes("yr")) {
            count -= 20;
          } else {
            count -= 2;
          }
        } else if (dataId == "3") {
          if (dataId == "3" && priceElement.textContent.includes("yr")) {
            count -= 20;
          } else {
            count -= 2;
          }
        }
        updater(count, labelElement, "", false, dataId);
      }
    }
  });
});

function updater(update, labelElement, data, boolean, dataId) {
  let totalParagraph = document.querySelector("p.total");
  let bElement = totalParagraph.querySelector("b");
  bElement.textContent = `${update}$`;

  let selectedAddonContainer = document.querySelector(".selected-addon");

  if (boolean === true) {
    if (!(dataId in selectedAddons)) {
      // Create a new container if it doesn't exist
      selectedAddons[dataId] = document.createElement("div");
    }

    let container = selectedAddons[dataId];
    let dynamictext = document.createElement("p");
    let dynamicprice = document.createElement("p");

    dynamictext.textContent = `${labelElement}`;
    dynamicprice.textContent = data;

    container.innerHTML = ""; // Clear container content before adding new items
    container.appendChild(dynamictext);
    container.appendChild(dynamicprice);

    selectedAddonContainer.appendChild(container);
  } else if (boolean === false && dataId in selectedAddons) {
    // Remove the selected addon container if it exists
    selectedAddonContainer.removeChild(selectedAddons[dataId]);
    delete selectedAddons[dataId]; // Remove it from the tracking object
  }
}

// Get all plan cards
let planCards = document.querySelectorAll(".plan-card");

// Iterate through each plan card
planCards.forEach(function (card) {
  card.addEventListener("click", function () {
    // Find the plan-priced span element within the clicked card
    let planPricedElement = card.querySelector(".plan-priced");
    if (planPricedElement) {
      let planPrice = planPricedElement.textContent.trim();
      let numericPart = planPrice.match(/\d+/);

      let numericValue = parseInt(numericPart[0], 10);
      console.log(numericValue);
      count = numericValue;
    }
  });
});
